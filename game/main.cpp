#include "../engine/Engine.h"

int main()
{
    Engine::Instance()->Start();

    Engine::Instance()->MainLoop();

    Engine::Instance()->CleanUp();

    return 0;
}
