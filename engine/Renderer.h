#ifndef RENDERER_H
#define RENDERER_H

#include "Shader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Renderer{
public:
    void Draw(float x, float y, float rotation, glm::vec3 color){
        glm::mat4 model;
        model = glm::translate(model, glm::vec3(x, y, 0.0f));
        model = glm::rotate(model, rotation, glm::vec3(0.0f,0.0f,1.0f));
        model = glm::translate(model, glm::vec3(-10.0f, -2.0f, 0.0f));
        model = glm::scale(model, glm::vec3(20.0f, 4.0f, 1.0f));
        shader.Use();
        shader.SetMatrix4("model", model);

        shader.SetVector3f("color", color);

        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
    }

    void initRenderData(){
        unsigned int VBO;
        float vertices[] = {
            0.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,

            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f
        };

        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glBindVertexArray(VAO);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (GLvoid*)0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        shader = Shader("shaders/default.vs", "shaders/default.fs");
        shader.Use();
        shader.SetMatrix4("projection", glm::ortho(0.0f,1600.0f,900.0f,0.0f,-1.0f,1.0f));
    }
private:
    Shader shader;
    unsigned int VAO;
};

#endif // RENDERER_H
