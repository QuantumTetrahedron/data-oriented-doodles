#include "Engine.h"
Engine* Engine::instance;

Engine* Engine::Instance(){
    if(!instance)
        instance = new Engine();
    return instance;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    Engine::Instance()->mouseX = x;
    Engine::Instance()->mouseY = y;
}

void Engine::Start(){
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    window = glfwCreateWindow(1600, 900, "Doodles", nullptr, nullptr);

    glfwMakeContextCurrent(window);

    glfwSetCursorPosCallback(window, mouse_callback);

    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    glGetError();

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    level.Load();
}

double currentFrame, frameTime, lastFrame = 0.0;

double frameTimeMin = 1.0, frameTimeMax = 0.0;

bool firstFrame = true;

void Engine::MainLoop(){
    while(!glfwWindowShouldClose(window))
    {
        currentFrame = glfwGetTime();
        frameTime = currentFrame - lastFrame;
        lastFrame = currentFrame;
        if(!firstFrame)
        {
            if(frameTime < frameTimeMin) frameTimeMin = frameTime;
            if(frameTime > frameTimeMax) frameTimeMax = frameTime;
        }
        else firstFrame = false;

        glfwPollEvents();

        level.Update(mouseX, mouseY);

        glClear(GL_COLOR_BUFFER_BIT);

        level.Render();

        glfwSwapBuffers(window);
    }
}

void Engine::CleanUp(){
    std::cout << "Frame time min: " << frameTimeMin << std::endl;
    std::cout << "Frame time max: " << frameTimeMax << std::endl;
    glfwTerminate();
}




