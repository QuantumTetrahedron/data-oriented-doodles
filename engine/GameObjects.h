#ifndef GAME_OBJECTS_H
#define GAME_OBJECTS_H

#include <vector>
#include "Renderer.h"

class GameObjects{
public:
    std::vector<float> xPositions;
    std::vector<float> yPositions;
    std::vector<float> rotations;
    std::vector<glm::vec3> colors;

    void AddObject(float xPos, float yPos){
        xPositions.push_back(xPos);
        yPositions.push_back(yPos);
        rotations.push_back(0.0f);
        colors.push_back(glm::vec3(1.0f));
    }

    void RenderAll(Renderer* renderer){
        for(int i = 0; i<xPositions.size(); ++i){
            renderer->Draw(xPositions[i], yPositions[i], rotations[i], colors[i]);
        }
    }

    void UpdateAll(float cursorX, float cursorY){
        for(int i = 0; i<rotations.size(); ++i){
            rotations[i] = glm::atan(yPositions[i]-cursorY,xPositions[i]-cursorX);
            colors[i] = glm::vec3(glm::abs(yPositions[i] - cursorY)/225.0f, glm::abs(xPositions[i] - cursorX)/400.0f, rotations[i]/360.0+0.5);
            colors[i] = glm::normalize(colors[i]);
        }
    }
};

#endif // GAME_OBJECTS_H
