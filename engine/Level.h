#ifndef LEVEL_H
#define LEVEL_H

#define __DATA_ORIENTED__

#include "GameObjects.h"
#include "GameObject.h"
#include "Renderer.h"

class Level{
public:
    #ifdef __DATA_ORIENTED__
    GameObjects objects;

    void Load(){
        for(int i = 1; i<128; ++i)
            for(int j = 1; j<72; ++j)
                objects.AddObject(12.5*i,12.5*j);
        renderer.initRenderData();
    }

    void Render(){
        objects.RenderAll(&renderer);
    }

    void Update(float mouseX, float mouseY){
        objects.UpdateAll(mouseX, mouseY);
    }
    #else
    std::vector<GameObject> objects;

    void Load(){
        for(int i = 1; i<128; ++i)
            for(int j = 1; j<72; ++j)
                objects.emplace_back(12.5*i, 12.5*j);
        renderer.initRenderData();
    }

    void Render(){
        for(GameObject& obj : objects)
            obj.Render(&renderer);
    }

    void Update(float mouseX, float mouseY){
        for(GameObject& obj : objects)
            obj.Update(mouseX, mouseY);
    }

    #endif
private:
    Renderer renderer;
};

#endif // LEVEL_H
