#ifndef ENGINE_H
#define ENGINE_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <memory>

#include "Level.h"

class Engine{
public:
    static Engine* Instance();

    void Start();

    void MainLoop();

    void CleanUp();
    double mouseX, mouseY;
private:
    Engine(){}
    static Engine* instance;

    GLFWwindow* window;

    Level level;

};

#endif // ENGINE_H
