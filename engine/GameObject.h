#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

class GameObject{
public:
    GameObject(float x, float y):xPosition(x),yPosition(y),rotation(0.0f),color(1.0f){}
    float xPosition;
    float yPosition;
    float rotation;
    glm::vec3 color;

    void Render(Renderer* renderer){
        renderer->Draw(xPosition, yPosition, rotation, color);
    }

    void Update(float cursorX, float cursorY){
        rotation = glm::atan(yPosition-cursorY,xPosition-cursorX);
        color = glm::vec3(glm::abs(yPosition - cursorY)/225.0f, glm::abs(xPosition - cursorX)/400.0f, rotation/360.0+0.5);
        color = glm::normalize(color);
    }
};

#endif // GAMEOBJECT_H
